package com.lms.ms.checkout;

import java.util.Date;
import java.util.function.Function;

import com.lms.ms.checkout.exception.InvalidAuthenticationDetailsException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtTokenUtils {

	private static String SECRETE_KEY = "1234";

	private static Claims extractAllClaims(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRETE_KEY).parseClaimsJws(token).getBody();
		return claims;
	}

	public String extractSubject(String token) {
		return extractClaim(token, Claims::getSubject);
	}

	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimExtractor) {
		Claims claims = extractAllClaims(token);
		return claimExtractor.apply(claims);

	}

	public boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public static Function<String, Claims> tokenValidator = (token) -> {
		Claims claims = extractAllClaims(token);
		Date expiration = claims.getExpiration();
		if (expiration.before(new Date())) {
			throw new InvalidAuthenticationDetailsException("The token got expired");
		}
		return claims;
	};

}
