package com.lms.ms.checkout;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.bson.Document;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;

@RequestScope
@Component
public class MongoConfig {

	Optional<MongoClient> client = Optional.empty();

	@PostConstruct
	public void mongoClient() {
		client = Optional.of(MongoClients.create("mongodb://localhost:27017"));
		MongoDatabase database = client.get().getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		collection.createIndex(Indexes.text("title"));

	}

	public MongoClient getMongoClient() {
		return client.get();
	}

	@PreDestroy
	public void clear() {
		client.ifPresent(MongoClient::close);
	}
}
