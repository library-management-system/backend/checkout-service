package com.lms.ms.checkout;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pmw.tinylog.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.google.common.net.HttpHeaders;
import com.lms.ms.checkout.exception.InvalidAuthenticationDetailsException;

import io.jsonwebtoken.Claims;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	RequestContext requestContext;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		Claims claims = null;
		final String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			claims = JwtTokenUtils.tokenValidator.apply(authorizationHeader.substring(7));

		}
		if (claims == null) {
			Logger.info("authentication failed");
			// Throw exception
			throw new InvalidAuthenticationDetailsException();
		}

		requestContext.setUserid(claims.getSubject());
		requestContext.setToken(authorizationHeader);

		return true;
	}
}
