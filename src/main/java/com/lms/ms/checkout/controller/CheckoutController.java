package com.lms.ms.checkout.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.ms.checkout.model.BorrowedBookModel;
import com.lms.ms.checkout.service.CheckoutService;

@RestController
@RequestMapping("/checkout/books/")
public class CheckoutController {

	@Autowired
	CheckoutService checkoutService;

	@GetMapping("/{id}")
	public ResponseEntity<HttpStatus> checkoutBooks(@PathVariable("id") String bookId) {
		checkoutService.checkout(Long.parseLong(bookId));
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	@GetMapping("/multiple/{bookIds}")
	public ResponseEntity<HttpStatus> checkoutBooks(@PathVariable("bookIds") List<Long> bookIds) {
		checkoutService.checkout(bookIds);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	@GetMapping("/status/{bookIds}")
	public ResponseEntity<Map<Long, BorrowedBookModel>> getStatusOfBooks(@PathVariable("bookIds") List<Long> bookIds) {
		Map<Long, BorrowedBookModel> status = checkoutService.getStatus(bookIds);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@GetMapping("/status/active/{userName}")
	public ResponseEntity<Map<Long, BorrowedBookModel>> getActiveBookOfUser(@PathVariable("userId") String userName) {
		Map<Long, BorrowedBookModel> status = checkoutService.getActiveBooksOfUser(userName);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

}
