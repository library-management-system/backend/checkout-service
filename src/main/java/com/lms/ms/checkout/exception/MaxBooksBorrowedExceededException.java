package com.lms.ms.checkout.exception;

public class MaxBooksBorrowedExceededException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MaxBooksBorrowedExceededException() {
		super("max books allowed exceeds");
	}

}
