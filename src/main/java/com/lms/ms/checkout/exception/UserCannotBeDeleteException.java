package com.lms.ms.checkout.exception;

public class UserCannotBeDeleteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserCannotBeDeleteException(String msg) {
		super(msg);
	}
}
