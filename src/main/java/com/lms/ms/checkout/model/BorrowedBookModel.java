package com.lms.ms.checkout.model;

public class BorrowedBookModel {

	private long bookId;
	private long borrowedAt;
	private String borrowedBy;
	private long dueDate;

	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	public long getBorrowedAt() {
		return borrowedAt;
	}

	public void setBorrowedAt(long borrowedAt) {
		this.borrowedAt = borrowedAt;
	}

	public String getBorrowedBy() {
		return borrowedBy;
	}

	public void setBorrowedBy(String borrowedBy) {
		this.borrowedBy = borrowedBy;
	}

	public long getDueDate() {
		return dueDate;
	}

	public void setDueDate(long dueDate) {
		this.dueDate = dueDate;
	}

}
