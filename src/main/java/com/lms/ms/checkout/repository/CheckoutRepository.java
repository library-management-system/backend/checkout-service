package com.lms.ms.checkout.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.ms.checkout.MongoConfig;
import com.lms.ms.checkout.RequestContext;
import com.lms.ms.checkout.exception.MaxBooksBorrowedExceededException;
import com.lms.ms.checkout.model.BorrowedBookModel;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertManyResult;

@Service
public class CheckoutRepository {

	public static final int MAX_BOOKS_ALLOWED_TO_BORROWED = 5;
	public static final int MAX_DUE_DATE = 10;

	public String userId = "afd";

	@Autowired
	RequestContext context;
	@Autowired
	MongoConfig config;

	ObjectMapper objectMapper = new ObjectMapper();

	@SuppressWarnings("unchecked")
	public void checkout(Set<Long> bookIds) {
		long activeBooks = getActiveBookOfUser(context.getUserid());
		if (activeBooks >= MAX_BOOKS_ALLOWED_TO_BORROWED) {
			throw new MaxBooksBorrowedExceededException();
		}

		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.DAY_OF_MONTH, MAX_DUE_DATE);

		long dueDate = calender.getTimeInMillis();
		List<BorrowedBookModel> models = new ArrayList<>();
		for (long bookId : bookIds) {
			BorrowedBookModel model = new BorrowedBookModel();
			model.setBookId(bookId);
			model.setBorrowedAt(System.currentTimeMillis());
			model.setDueDate(dueDate);
			model.setBorrowedBy(context.getUserid());
			models.add(model);
		}

		MongoCollection<Document> collection = getActiveBooksCollection();
		List<Map<String, Object>> modelsMap = objectMapper.convertValue(models, List.class);
		List<Document> borrowedBooksDocuments = new ArrayList<>();
		modelsMap.forEach(modelMap -> {
			long bookId = (long) modelMap.get("bookId");
			modelMap.put("_id", bookId);
			borrowedBooksDocuments.add(new Document(modelMap));
		});

		InsertManyResult result = collection.insertMany(borrowedBooksDocuments);
		if (!result.wasAcknowledged()) {
			// TODO : throw exception
		}

	}

	public Map<Long, BorrowedBookModel> getStatusOfBooks(Set<Long> bookIds) {
		MongoCollection<Document> collection = getActiveBooksCollection();
		FindIterable<Document> queryResults = collection.find(Filters.in("bookId", bookIds));
		MongoCursor<Document> cursor = queryResults.cursor();
		Map<Long, BorrowedBookModel> bookStatus = new HashMap<>();
		while (cursor.hasNext()) {
			Map<String, Object> document = cursor.next();
			document.remove("_id");
			BorrowedBookModel model = objectMapper.convertValue(document, BorrowedBookModel.class);
			bookStatus.put(model.getBookId(), model);
		}
		return bookStatus;
	}

	private MongoCollection<Document> getActiveBooksCollection() {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		return database.getCollection("active-books");
	}

	private long getActiveBookOfUser(String userId) {
		MongoCollection<Document> collection = getActiveBooksCollection();
		long totalBooksBorrowed = collection.countDocuments(Filters.eq("borrowedBy", userId));
		return totalBooksBorrowed;
	}

	public Map<Long, BorrowedBookModel> getAllActiveBookOfUser(String userName) {
		MongoCollection<Document> collection = getActiveBooksCollection();
		FindIterable<Document> queryResults = collection.find(Filters.eq("borrowedBy", userName));
		Map<Long, BorrowedBookModel> bookStatus = new HashMap<>();
		MongoCursor<Document> cursor = queryResults.cursor();
		while (cursor.hasNext()) {
			Map<String, Object> document = cursor.next();
			document.remove("_id");
			BorrowedBookModel model = objectMapper.convertValue(document, BorrowedBookModel.class);
			bookStatus.put(model.getBookId(), model);
		}
		return bookStatus;
	}

}
