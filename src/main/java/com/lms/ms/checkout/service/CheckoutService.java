package com.lms.ms.checkout.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.ms.checkout.model.BorrowedBookModel;
import com.lms.ms.checkout.repository.CheckoutRepository;

@Service
public class CheckoutService {

	@Autowired
	CheckoutRepository checkoutRepo;

	public void checkout(long bookId) {
		checkoutRepo.checkout(Set.of(bookId));
	}

	public void checkout(List<Long> bookIds) {
		checkoutRepo.checkout(new HashSet<>(bookIds));
	}

	public Map<Long, BorrowedBookModel> getStatus(List<Long> bookIds) {
		return checkoutRepo.getStatusOfBooks(new HashSet<>(bookIds));
	}

	public Map<Long, BorrowedBookModel> getActiveBooksOfUser(String userName) {
		return checkoutRepo.getAllActiveBookOfUser(userName);
	}
}
